use music_db;

--A
SELECT * FROM artists WHERE name LIKE "%d%";

--B
SELECT * FROM songs WHERE length < 230;

--C
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON songs.id = albums.artist_id;

--D
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- E
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--D
SELECT * FROM albums JOIN songs ON songs.id = albums.artist_id ORDER BY albums.album_title DESC, songs.song_name ASC;